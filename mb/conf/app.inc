location /api {
	content_by_lua_file "$gml/$appmc/lua/api.lua";
}

location @cw {
	content_by_lua_file "$gml/$appmc/lua/cw.lua";
}

location /	{
	root $gml/$appmc/static;
	index index.htm;
}
