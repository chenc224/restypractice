local cc

local ora={
    connect=function(self)  --连接到数据库
        for i,v in pairs(self.localconfig.initenv or {}) do
            cc.posix.setenv(i,v)
        end
        self.driver = self.driver or require "luasql.oci8"
        self.env = assert (self.driver.oci8())
        if self.connected then return end   --如果已经连接则不再重新连接
        local yh,mm,fwq=self:convert(self.localconfig.connect_conf):match("([^/]+)/([^@]+)@(.+)")
        self.conn = self.env:connect(fwq,yh,mm)
        if self.conn then 
            self.connected=true
            for i,sql in pairs(self.localconfig.initsql or {}) do
                self:xg(sql)
            end
        end
    end,

    convert=function(self,ljc)  --转换联接串
        if ljc:find("/") then return ljc end
        local f=assert(io.open("/etc/oraconn.cfg","r"))
        local jg
        for nr in f:lines() do
            local s1,s2=nr:match("(%S+)%s+(%S+)")
            if s1==ljc then jg=s2 end
        end
        f:close()
        return jg or ""
    end,

    execute=function(self,sql,...)  --执行一条命令
        if not self.connected then self:connect() end
        sql=self:fmtsql(sql,...)
        return self.conn:execute(sql)
    end,

    jg1=function(self,sql,...)  --返回一个查询结果
        if not self.connected then self:connect() end
        sql=self:fmtsql(sql,...)
        local cur = assert (self.conn:execute(sql))
        local jg=cur:fetch({})
        cur:close()
        if not jg then return end
        return unpack(jg)
    end,

    jgs=function(self,sql,...)  --for迭代器，用于返回一行一行的结果
        if not self.connected then self:connect() end
        sql=self:fmtsql(sql,...)
        local cur = assert (self.conn:execute(sql))
        return cur.fetch,cur
    end,

    new=function(self,localconfig)  --创建一个数据库连接
        local o={}
        setmetatable(o,self)
        self.__index=self
        o:setconfig(localconfig)
        return o
    end,

    setconfig=function(self,localconfig)   --设置连接串
        self.localconfig=localconfig
        self.connected=false
    end,

    xg=function(self,sql,...)   --执行并commit
        self:execute(sql,...)
        self:execute("commit")
    end,
    
    fmtsql=function(self,sql,...)
        if select("#",...)>0 then
            local t={}
            for i,v in ipairs({...}) do
                if type(v)=="string" then
                    t[i]="'" .. v:gsub("'","''") .. "'"
                else
                    t[i]=v
                end
            end
            sql=sql:format(unpack(t))
        end
        return sql
    end,
    wclob=function(self,表名,字段名,clob数据,是gbk,where条件,...)
        where条件=self:fmtsql(where条件,...)
        local 数据=cc:拆分(clob数据,是gbk,1800)
        self:execute("update " .. 表名 .. " set " .. 字段名 .. "=null where " .. where条件)
        for _,s in pairs(数据) do
            self:execute("update " .. 表名 .. " set " .. 字段名 .. "=nvl(" .. 字段名 .. ",'')||to_clob(%s) where " .. where条件,s)
        end
    end,
}

local my={  --mysql库
    connect=function(self)  --连接到数据库
        local ok,err,errno,sqlstate = self.db:connect (self.conf.connect_conf)
        if not ok then
            cc:rz({"mysql connect error:",err,errno,sqlstate})
            ngx.exec("@cw")
        end
        self.connected=true
        for i,v in pairs(self.conf.initsql or {}) do
            self:query(v)
        end
    end,

    execute=function(self,sql,...)  --执行一条命令
        if not self.connected then self:connect() end
        sql=self:fmtsql(sql,...)
        self.conn:execute(sql)
    end,

    new=function(self,localconfig)  --创建一个对象实例
        local mysql=require "resty.mysql"
        local db,err = mysql:new()
        if not db then
            cc:rz("mysql lib init error")
            ngx.exec("@cw")
        end
        db:set_timeout(1000)
        local o={}
        setmetatable(o,self)
        self.__index=self
        o:setconfig(localconfig)
        o.db=db
        return o
    end,

    querys=function(self,sql,...)   --返回多个结果
        if not self.connected then self:connect() end
        sql=self:fmtsql(sql,...)
        self.res,self.dberr,self.dberrno,self.sqlstate = self.db:query(sql)
        if not self.res then
            cc:rz({sql,self.dberr,self.dberrno,self.sqlstate})
        end
        return self.res
    end,

    query=function(self,sql,...)    --如果是单一结果，返回结果集本身
        local res=self:querys(sql,...)
        if not res then return end
        if #res==0 then return res end
        if #res==1 then
            return res[1]
        end
        return res
    end,

    setconfig=function(self,localconfig)   --设置连接串
        self.conf=localconfig
        self.connected=false
    end,

    xg=function(self,sql,...)   --执行并commit
        self:execute(sql,...)
        self:execute("commit")
    end,
    
    fmtsql=function(self,sql,...)
        if select("#",...)>0 then
            local t={}
            for i,v in ipairs({...}) do
                if type(v)=="string" then
                    t[i]="'" .. v:gsub("'","''") .. "'"
                else
                    t[i]=v
                end
            end
            sql=sql:format(unpack(t))
        end
        return sql
    end,
}

cc = {
    cwxx,cwdm='',0 ,
    localset=require (ngx.var.appmc .. ".lua.localset"),
    cjson=require "cjson",
    ora=ora,
    cw=function(self,cwxx,cwdm)
        self.cwdm=cwdm or 0
        self.cwxx=cwxx or ""
        return cwdm
    end,
    checklogin=function(self)   --检查是否登录，未登录切换到登录窗口
        self.yhid=tonumber(ngx.var.cookie_yh)
        self.fdid=ngx.var.cookie_fd
        if self.yhid and self.fdid then
            jg=self:query("select count(*) sl from zxyh where yhid=%d and fdid=%s",self.yhid,self.fdid)
            if jg and jg.sl=="1" then
                self:query("update zxyh set zhfwsj=now() where yhid=%d and fdid=%s",self.yhid,self.fdid)
                self.yhcs2=self:query("select * from yhcs2 where yhid=%d",self.yhid)
                return
            end
        end
        ngx.redirect("/login")
    end,
    dbinit=function(self,xh)    --根据序号初始化数据库连接，如果未指定序号则初始化所有
        ngx.ctx.db={}
        for i,v in pairs(self.localset.db or {}) do
            if ((not xh ) or xh==i ) and v.db and self["initdb_" .. v.db] then
                self["db".. i]=self["initdb_" .. v.db](self,v)
                ngx.ctx.db[i]=self["db".. i]
            end
        end
        self.db=self["db1"]
    end,
    init=function(self)
        local cjson2=self.cjson.new()
        self.posix=require "posix"
        self.json=require "cjson.safe"
        self.sd=ngx.shared.shared_data
        self:dbinit()
        return ngx.ctx.db[1]
--      return self.db
    end,
    initdb_mysql=function(self,conf)
        local mysql=require "resty.mysql"
        local db,err = mysql:new()
        if not db then
            self:rz("mysql lib init error")
            ngx.exec("@cw")
        end
        db:set_timeout(100000)
        local ok,err,errno,sqlstate = db:connect (conf.connect_conf)
        if not ok then
            self:rz({"mysql connect error:",err,errno,sqlstate})
            ngx.exec("@cw")
        end
        
        for i,v in pairs(conf.initsql or {}) do
            db:query(v)
        end
        return db
    end,
    initdb_oracle=function(self,conf)
        return ora:new(conf)
    end,
    initdb_mysql2=function(self,conf)
        return my:new(conf)
    end,
    quit=function(self,msg,...)
        if select("#",...)>0 then msg=string.format(msg, ...) end
        if msg and msg~="" then ngx.say(msg) end
        if self.db.set_keepalive then
            local ok,err=self.db:set_keepalive(10000,100)
        end
        ngx.exit(ngx.OK)
    end,
    rz=function(self,msg,...)   --输出日志信息
        if select("#",...)>0 then msg=string.format(msg, ...) end
        if type(msg)=="table" then msg=self.cjson.encode(msg) end
        local s=debug.getinfo(2)
        msg=s and string.format("%s:%s:%s",s.short_src,s.currentline,msg) or msg
        if self.rz2say then
            ngx.say(msg)
        else
            ngx.log(ngx.ERR,msg)
        end
    end,
    disparg=function(self) --显示所有get、post等参数
        ngx.req.read_body()
        local postargs = ngx.req.get_post_args()
        for k,v in pairs(postargs or {}) do
            if v==true then
                self:rz(self.json.decode(k))
                break
            end
            self:rz(k .. ":" .. v)
        end
    end,
    querys=function(self,sql,...)   --返回多个结果
        if select("#",...)>0 then
            local t={}
            for i,v in ipairs({...}) do
                if type(v)=="string" then
                    t[i]=ngx.quote_sql_str(v)
                else
                    t[i]=v
                end
            end
            sql=sql:format(unpack(t))
        end
        self.res,self.dberr,self.dberrno,self.sqlstate = self.db:query(sql)
        if not self.res then
            self:rz({sql,self.dberr,self.dberrno,self.sqlstate})
        end
        return self.res
    end,
    query=function(self,sql,...)    --如果是单一结果，返回结果集本身
        local res=self:querys(sql,...)
        if not res then return end
        if #res==0 then return end
        if #res==1 then
            return res[1]
        end
        return res
    end,
    getarg=function(self,...)
        ngx.req.read_body()
        local postargs = ngx.req.get_post_args()
        local t={}
        for i,v in ipairs({...}) do
            t[i]=postargs[v]
            if not t[i] then return end
        end
        return unpack(t)
    end,
    clientip=function(self)
        local headers=ngx.req.get_headers()
        local clientIP = headers["x-forwarded-for"]
        if clientIP == nil or string.len(clientIP) == 0 or clientIP == "unknown" then
            clientIP = headers["Proxy-Client-IP"]
        end
        if clientIP == nil or string.len(clientIP) == 0 or clientIP == "unknown" then
            clientIP = headers["WL-Proxy-Client-IP"]
        end
        if clientIP == nil or string.len(clientIP) == 0 or clientIP == "unknown" then
            clientIP = ngx.var.remote_addr
        end
        -- 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if clientIP ~= nil and string.len(clientIP) >15  then
            local pos  = string.find(clientIP, ",", 1)
            clientIP = string.sub(clientIP,1,pos-1)
        end
        return clientIP
    end,
    disppost=function(self)
        ngx.req.read_body()
        self:rz(ngx.req.get_post_args())
    end,
    s2num=function(self,...)
        local t={}
        for i,v in ipairs({...}) do
            t[i]=tonumber(v)
        end
        return unpack(t)
    end,
    togbk=function(self,utf8str)
        local iconv = require("iconv")
        local togbk = iconv.new("gbk","utf-8")
        local str, err = togbk:iconv(utf8str)
        return str
    end,
    toutf8=function(self,gbkstr)
        local iconv = require("iconv")
        local toutf8 = iconv.new("utf-8","gbk")
        local str, err = toutf8:iconv(gbkstr)
        return str
    end,
    uuid=function(self)
        local f=assert(io.open("/proc/sys/kernel/random/uuid","r"))
        local jg=f:read():gsub("-","")
        f:close()
        return jg
    end,
    new=function(self)  --创建一个对象实例
        local o={}
        setmetatable(o,self)
        self.__index=self
        local cjson2=self.cjson.new()
        o.posix=require "posix"
        o.json=require "cjson.safe"
        o.sd=ngx.shared.shared_data
        o:dbinit()
        return o
    end,
    拆分字符=function(self,数据,是gbk)
        local f=function(_,i)
            i=i+1
            if i>#数据 then return end
            local c=数据:byte(i)
            if 是gbk then
                if c>127 then return i+1,数据:sub(i,i+1) end
                return i,数据:sub(i,i)
            else
                if c>239 then return i+3,数据:sub(i,i+3) end
                if c>223 then return i+2,数据:sub(i,i+2) end
                if c>128 then return i+1,数据:sub(i,i+1) end
                return i,数据:sub(i,i)
            end
        end
        return f,_,0
    end,
    拆分=function(self,数据,是gbk,长度)
        local 结果,子串,子串长度={},{},0
        for 位置,字符 in self:拆分字符(数据,是gbk) do
            if 子串长度+#字符>长度 then
                结果[#结果+1]=table.concat(子串)
                子串,子串长度={},0
            end
            子串[#子串+1]=字符
            子串长度=子串长度+#字符
        end
        if 子串长度>0 then
            结果[#结果+1]=table.concat(子串)
        end
        return 结果
    end,
}

return cc
